﻿using AutoMapper;
using BSA.Common.DTO.Team;
using BSA.BL.Exceptions;
using BSA.Common.MappingProfiles;
using BSA.BL.Services;
using BSA.Repo.Context;
using BSA.Repo.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BSA.BL.Tests
{
    public class TeamServiceTests
    {
        private BsaDbContext _ctx;
        private TeamService _teamService;
        private ProjectService _projService;

        public TeamServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<BsaDbContext>()
                                .UseInMemoryDatabase(databaseName: "Test")
                                .Options;
            _ctx = new BsaDbContext(dbOptions);

            _ctx.Database.EnsureDeleted();
            _ctx.Database.EnsureCreated();

            var uow = new UnitOfWork(_ctx);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });
            var mapper = new Mapper(mapperConfig);
            _teamService = new TeamService(uow, mapper);
            _projService = new ProjectService(uow, mapper);
        }

        public void Dispose()
        {
            _ctx.Database.EnsureDeleted();
            _ctx.Dispose();
        }

        [Fact]
        public async Task GetAllTeams_WhenSeedData_ThenCountEqual10()
        {
            var teams = await _teamService.GetAllTeams();

            Assert.Equal(10, teams.Count());
        }

        [Theory]
        [InlineData(2)]
        [InlineData(5)]
        [InlineData(7)]
        public async Task GetTeamsById_WhenSeedDataAndTeamExist_ThenTeamDTOWithId(int id)
        {
            var team = await _teamService.GetTeamById(id);

            Assert.NotNull(team);
            Assert.Equal(id, team.Id);
        }

        [Theory]
        [InlineData(20)]
        [InlineData(500)]
        [InlineData(721)]
        public async Task GetTeamById_WhenSeedDataAndTeamDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            await Assert.ThrowsAsync<NotFoundException>(async () => await _teamService.GetTeamById(id));
        }

        [Fact]
        public async Task AddTeam_WhenSeeddataAndNewTeamDTOIsValid_ThenReturnTeamDTO()
        {
            var team = new NewTeamDTO
            {
                Name = "Test"
            };

            var addedTeam = await _teamService.AddTeam(team);

            Assert.NotNull(addedTeam);
            Assert.Equal(team.Name, addedTeam.Name);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        [InlineData(7)]
        public async Task UpdateTeam_WhenSeedDataAndValidNewTeamDTOAndTeamExist_ThenTeamDTOWithNewName(int id)
        {
            var prevTeam = await _teamService.GetTeamById(id);
            var team = new NewTeamDTO
            {
                Name = "Name"
            };

            await _teamService.UpdateTeam(team, id);
            var newTeam = await _teamService.GetTeamById(id);

            Assert.NotEqual(prevTeam.Name, newTeam.Name);
        }

        [Theory]
        [InlineData(1004)]
        [InlineData(1404)]
        [InlineData(int.MaxValue)]
        public async Task UpdateTeam_WhenSeedDataAndValidNewTeamDTOAndTeamDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            var team = new NewTeamDTO
            {
                Name = "Name"
            };

            await Assert.ThrowsAsync<NotFoundException>(async () => await _teamService.UpdateTeam(team, id));
        }

        [Theory]
        [InlineData(2)]
        [InlineData(7)]
        [InlineData(5)]
        public async Task DeleteTeam_WhenSeedDataAndTeamExistWithId_ThenNotFoundExeptionOnGetTeamByIdAndCountOfProjectsWithTeamIdEquals0(int id)
        {
            await _teamService.DeleteTeam(id);
            var projects = (await _projService.GetAllProjects()).Where(p => p.TeamId == id);

            await Assert.ThrowsAsync<NotFoundException>(async () => await _teamService.GetTeamById(id));
            Assert.Empty(projects);
        }

        [Theory]
        [InlineData(73285)]
        [InlineData(2351)]
        [InlineData(int.MaxValue)]
        public async Task DeleteTeam_WhenSeedDataAndTeamDoesNotExist_ThrowNotFoundExeption(int id)
        {
            await Assert.ThrowsAsync<NotFoundException>(async () => await _teamService.DeleteTeam(id));
        }

        [Fact]
        public async Task GetTeamsWithUsersOlderThanTenYears_WhenSeedData_ThenCollectionOfTeamWithUsersDTOWhereUsersOlder10()
        {
            var result = await _teamService.GetTeamsWithUsersOlderThanTenYears();

            Assert.DoesNotContain(result, r => r.Users.Any(u => CalculateAge(u.Birthday) < 10));
        }

        private int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }
    }
}
