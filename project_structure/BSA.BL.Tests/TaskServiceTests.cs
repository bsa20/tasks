﻿using AutoMapper;
using BSA.Common.DTO.Task;
using BSA.BL.Exceptions;
using BSA.Common.MappingProfiles;
using BSA.BL.Services;
using BSA.Repo.Context;
using BSA.Repo.Enums;
using BSA.Repo.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BSA.BL.Tests
{
    public class TaskServiceTests
    {
        private BsaDbContext _ctx;
        private Mapper _mapper;
        private TaskService _taskService;

        public TaskServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<BsaDbContext>()
                                .UseInMemoryDatabase(databaseName: "Test")
                                .Options;
            _ctx = new BsaDbContext(dbOptions);

            _ctx.Database.EnsureDeleted();
            _ctx.Database.EnsureCreated();

            var uow = new UnitOfWork(_ctx);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });
            _mapper = new Mapper(mapperConfig);
            _taskService = new TaskService(uow, _mapper);
        }

        public void Dispose()
        {
            _ctx.Database.EnsureDeleted();
            _ctx.Dispose();
        }

        [Fact]
        public async Task GetAllTasks_WhenSeedData_ThenCountEquals200()
        {
            var tasks = await _taskService.GetAllTasks();

            Assert.Equal(200, tasks.Count());
        }

        [Theory]
        [InlineData(23)]
        [InlineData(91)]
        [InlineData(175)]
        public async Task GetTaskById_WhenSeedDataAndTaskExist_ThenTaskDTOWithId(int id)
        {
            Assert.Equal(id, (await _taskService.GetTaskById(id)).Id);
        }

        [Theory]
        [InlineData(2302)]
        [InlineData(9174382)]
        [InlineData(int.MaxValue)]
        public async Task GetTaskById_WhenSeedDataAndTaskDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            await Assert.ThrowsAsync<NotFoundException>(async () => await _taskService.GetTaskById(id));
        }

        [Fact]
        public async Task AddTAsk_WhenSeedDataAndNewTaskDTOValid_ThenTaskDTOWithAlEnteredProperties()
        {
            var task = new NewTaskDTO
            {
                Name = "Name",
                Description = "Description",
                PerformerId = 1,
                ProjectId = 5,
                FinishedAt = new DateTime(2021, 5, 7),
                State = TaskState.Created
            };

            var newTask = await _taskService.AddTask(task);

            Assert.Equal(task.Name, newTask.Name);
            Assert.Equal(task.Description, newTask.Description);
            Assert.Equal(task.PerformerId, newTask.PerformerId);
            Assert.Equal(task.ProjectId, newTask.ProjectId);
            Assert.Equal(task.FinishedAt, newTask.FinishedAt);
        }

        [Theory]
        [InlineData(1,1000000)]
        [InlineData(1000000, 1)]
        [InlineData(10000000, 10000000)]
        public async Task AddTask_WhenSeedDataAndNewTaskDTONotValid_ThenThrowNotFoundExeption(int performerId, int projectId)
        {
            var task = new NewTaskDTO
            {
                Name = "Name",
                Description = "Description",
                PerformerId = performerId,
                ProjectId = projectId,
                FinishedAt = new DateTime(2021, 5, 7),
                State = TaskState.Created
            };

            await Assert.ThrowsAsync<NotFoundException>(async() => await _taskService.AddTask(task));
        }

        [Fact]
        public async Task UpdateTask_WhenSeedDataAndNewTaskDTOValidAndTeamExist_ThenTaskDTOWithUpdatedData()
        {
            var task = new NewTaskDTO
            {
                Name = "Name",
                Description = "Description",
                PerformerId = 1,
                ProjectId = 5,
                FinishedAt = new DateTime(2021, 5, 7),
                State = TaskState.Created
            };

            await _taskService.UpdateTask(task, 1);
            var newTask = await _taskService.GetTaskById(1);

            Assert.Equal(task.Name, newTask.Name);
            Assert.Equal(task.Description, newTask.Description);
            Assert.Equal(task.PerformerId, newTask.PerformerId);
            Assert.Equal(task.ProjectId, newTask.ProjectId);
            Assert.Equal(task.FinishedAt, newTask.FinishedAt);
        }


        [Theory]
        [InlineData(5)]
        [InlineData(58)]
        [InlineData(176)]
        public async Task UpdateTask_SetFinished_WhenSeedDataAndNewTaskDTOValidAndTeamExist_ThenTaskDTOWithStateFinished(int id)
        {
            var oldTask = await _taskService.GetTaskById(id);
            oldTask.State = TaskState.Finished;
            var task = _mapper.Map<NewTaskDTO>(oldTask);

            await _taskService.UpdateTask(task, id);
            var newTask = await _taskService.GetTaskById(id);

            Assert.Equal(TaskState.Finished, newTask.State);
        }

        [Theory]
        [InlineData(5, 1, 1000000)]
        [InlineData(137, 1000000, 1)]
        [InlineData(111, 10000000, 10000000)]
        public async Task UpdateTask_SetFinished_WhenSeedDataAndNewTaskDTONotValidAndTeamExist_ThenNotFoundExeption(int id, int performerId, int projectId)
        {
            var oldTask = await _taskService.GetTaskById(id);
            var task = _mapper.Map<NewTaskDTO>(oldTask);
            task.PerformerId = performerId;
            task.ProjectId = projectId;

            await Assert.ThrowsAsync<NotFoundException>(async () => await _taskService.UpdateTask(task, id));
        }

        [Theory]
        [InlineData(5)]
        [InlineData(58)]
        [InlineData(176)]
        public async Task DeleteTask_WhenSeedDataAndTaskExist_ThenNotFoundExeptionOnGetTaskById(int id)
        {
            await _taskService.DeleteTask(id);

            await Assert.ThrowsAsync<NotFoundException>(async () => await _taskService.GetTaskById(id));
        }

        [Theory]
        [InlineData(500)]
        [InlineData(58001)]
        [InlineData(int.MaxValue)]
        public async Task DeleteTask_WhenSeedDataAndTaskDoesNotExist_ThenNotFoundExeptionOnGetTaskById(int id)
        {
            await Assert.ThrowsAsync<NotFoundException>(async () => await _taskService.DeleteTask(id));
        }
    }
}
