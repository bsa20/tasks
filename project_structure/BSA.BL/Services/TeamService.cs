﻿using AutoMapper;
using BSA.BL.Exceptions;
using BSA.Common.DTO.Team;
using BSA.Common.DTO.User;
using BSA.Repo.Models;
using BSA.Repo.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.BL.Services
{
    public class TeamService
    {
        private readonly UnitOfWork _uow;
        private readonly IMapper _mapper;

        public TeamService(UnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TeamDTO>> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(await _uow.Teams.GetAll());
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var team = await _uow.Teams.GetById(id);
            if (team == null)
                throw new NotFoundException(nameof(Team), id);

            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<TeamDTO> AddTeam(NewTeamDTO team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            await _uow.Teams.Add(teamEntity);

            await _uow.SaveAsync();

            var newTeam = await _uow.Teams.GetById(teamEntity.Id);
            return _mapper.Map<TeamDTO>(newTeam);
        }

        public async Task UpdateTeam(NewTeamDTO team, int id)
        {
            var teamCheck = await _uow.Teams.GetById(id);
            if (teamCheck == null)
                throw new NotFoundException(nameof(Team), id);
            _uow.DetachEntity(teamCheck);

            var teamEntity = _mapper.Map<Team>(team);
            teamEntity.Id = id;
            teamEntity.CreatedAt = teamCheck.CreatedAt;

            await _uow.Teams.Update(teamEntity);
            await _uow.SaveAsync();
        }

        public async Task DeleteTeam(int id)
        {
            var team = await _uow.Teams.GetById(id);
            if (team == null)
                throw new NotFoundException(nameof(Team), id);

            await _uow.Teams.Remove(id);
            await _uow.SaveAsync();
        }

        // 4
        public async Task<IEnumerable<TeamWithUsersDTO>> GetTeamsWithUsersOlderThanTenYears()
        {
            var teams = await _uow.Teams.GetAll();
            var users = await _uow.Users.GetAll();

            return teams.GroupJoin(users.Where(u => CalculateAge(u.Birthday) > 10), t => t.Id, u => u.TeamId, (t, u) => new TeamWithUsersDTO
            {
                Id = t.Id,
                Name = t.Name,
                Users = _mapper.Map<IEnumerable<UserDTO>>(u.OrderByDescending(u => u.RegisteredAt).ToList())
            }).Where(t => t.Users.Any()).ToList();
        }

        private int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }
    }
}
