﻿using AutoMapper;
using BSA.BL.Exceptions;
using BSA.Common.DTO.Project;
using BSA.Common.DTO.Task;
using BSA.Repo.Models;
using BSA.Repo.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.BL.Services
{
    public class ProjectService
    {
        private readonly UnitOfWork _uow;
        private readonly IMapper _mapper;

        public ProjectService(UnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(await _uow.Projects.GetAll());
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var proj = await _uow.Projects.GetById(id);
            if (proj == null)
                throw new NotFoundException(nameof(Project), id);

            return _mapper.Map<ProjectDTO>(proj);
        }

        public async Task<ProjectDTO> AddProject(NewProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);

            var team = await _uow.Teams.GetById(project.TeamId);
            if (team == null)
                throw new NotFoundException(nameof(Team), project.TeamId);
            var user = await _uow.Users.GetById(project.AuthorId);
            if (user == null)
                throw new NotFoundException(nameof(User), project.AuthorId);

            await _uow.Projects.Add(projectEntity);

            await _uow.SaveAsync();

            var proj = await _uow.Projects.GetById(projectEntity.Id);
            return _mapper.Map<ProjectDTO>(proj);
        }

        public async Task UpdateProject(NewProjectDTO project, int id)
        {
            var proj = await _uow.Projects.GetById(id);
            if (proj == null)
                throw new NotFoundException(nameof(Project), id);
            if (await _uow.Teams.GetById(project.TeamId) == null)
                throw new NotFoundException(nameof(Team), project.TeamId);
            if (await _uow.Users.GetById(project.AuthorId) == null)
                throw new NotFoundException(nameof(User), project.AuthorId);
            _uow.DetachEntity(proj);

            var projectEntity = _mapper.Map<Project>(project);
            projectEntity.Id = id;
            projectEntity.CreatedAt = proj.CreatedAt;

            await _uow.Projects.Update(projectEntity);
            await _uow.SaveAsync();
        }

        public async Task DeleteProject(int id)
        {
            var proj = await _uow.Projects.GetById(id);
            if (proj == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }

            await _uow.Projects.Remove(id);
            await _uow.SaveAsync();
        }

        // 7
        public async Task<IEnumerable<ProjectMainInfoDTO>> GetMainInfoAboutProjects()
        {
            var projects = await _uow.Projects.GetAll();
            var users = await _uow.Users.GetAll();

            return projects.Select(p => new ProjectMainInfoDTO
            {
                Project = _mapper.Map<ProjectDTO>(p),
                LongestTaskByDescription = _mapper.Map<TaskDTO>(p.Tasks.Count() == 0 ? null : p.Tasks.Aggregate((max, t) => (t.Description.Length > max.Description.Length ? t : max))),
                ShortetTaskByName = _mapper.Map<TaskDTO>(p.Tasks.Count() == 0 ? null : p.Tasks.Aggregate((min, t) => (t.Name.Length < min.Name.Length ? t : min))),
                NumberOfUsersInTeam = (p.Description.Length > 20 || p.Tasks.Count() < 3) ? users.Count(u => u.TeamId == p.TeamId) : 0
            }).ToList();
        }
    }
}
