﻿using AutoMapper;
using BSA.BL.Exceptions;
using BSA.Common.DTO.Project;
using BSA.Common.DTO.Task;
using BSA.Common.DTO.User;
using BSA.Repo.Enums;
using BSA.Repo.Models;
using BSA.Repo.UnitOfWork;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.BL.Services
{
    public class UserService
    {
        private readonly UnitOfWork _uow;
        private readonly IMapper _mapper;

        public UserService(UnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(await _uow.Users.GetAll());
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var user = await _uow.Users.GetById(id);
            if (user == null)
                throw new NotFoundException(nameof(User), id);

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> AddUser(NewUserDTO user)
        {
            var userEntity = _mapper.Map<User>(user);

            if (user.TeamId != null)
            {
                var team = await _uow.Teams.GetById((int)user.TeamId);
                if (team == null)
                    throw new NotFoundException(nameof(Team), (int)user.TeamId);
            }

            await _uow.Users.Add(userEntity);
            await _uow.SaveAsync();

            var newUser = await _uow.Users.GetById(userEntity.Id);
            return _mapper.Map<UserDTO>(newUser);
        }

        public async Task UpdateUser(NewUserDTO user, int id)
        {
            var userCheck = await _uow.Users.GetById(id);
            if (userCheck == null)
                throw new NotFoundException(nameof(User), id);
            if (user.TeamId != null)
            {
                var team = await _uow.Teams.GetById((int)user.TeamId);
                if (team == null) throw new NotFoundException(nameof(Team), (int)user.TeamId);
            }
            _uow.DetachEntity(userCheck);

            var userEntity = _mapper.Map<User>(user);
            userEntity.Id = id;
            userEntity.RegisteredAt = userCheck.RegisteredAt;

            await _uow.Users.Update(userEntity);
            await _uow.SaveAsync();
        }

        public async Task DeleteUser(int id)
        {
            var user = await _uow.Users.GetById(id);
            if (user == null)
                throw new NotFoundException(nameof(User), id);

            await _uow.Users.Remove(id);
            await _uow.SaveAsync();
        }

        // 6
        public async Task<UserMainInfoDTO> GetMainInformationAboutUser(int userId)
        {
            var projects = await _uow.Projects.GetAll();
            var user = await _uow.Users.GetById(userId);
            var tasks = await _uow.Tasks.GetAll();

            return new UserMainInfoDTO
            {
                User = _mapper.Map<UserDTO>(user),
                LastProject = _mapper.Map<ProjectDTO>(projects.Any(p => p.AuthorId == userId) ?
                                                      projects.Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).First() : null),
                NumberOfTasksOfLastProject = projects.Any(p => p.AuthorId == userId) ?
                                             projects.Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).First().Tasks.Count() : 0,
                NumberOfNotFinishedTasks = tasks.Count(t => t.PerformerId == userId && t.State != TaskState.Finished),
                LongestTask = _mapper.Map<TaskDTO>(tasks.Where(t => t.PerformerId == userId)
                                     .Aggregate((max, t) => (t.FinishedAt - t.CreatedAt > max.FinishedAt - max.CreatedAt ? t : max)))
            };
        }

        // 5
        public async Task<IEnumerable<UserWithTasksDTO>> GetSortedUsersWithSortedTasks()
        {
            var users = await _uow.Users.GetAll();
            var tasks = await _uow.Tasks.GetAll();

            return users.OrderBy(u => u.FirstName)
                .GroupJoin(tasks, u => u.Id, t => t.PerformerId, (u, t) => new UserWithTasksDTO
                {
                    User = _mapper.Map<UserDTO>(u),
                    Tasks = _mapper.Map<IEnumerable<TaskDTO>>(t.OrderByDescending(t => t.Name.Length).ToList())
                }).ToList();
        }

        // 1
        public async Task<IDictionary<int, int>> GetCountOfTasksOfUserInProject(int userId)
        {
            var projects = await _uow.Projects.GetAll();

            return projects.ToDictionary(p => p.Id, t => t.Tasks.Count(ts => ts.PerformerId == userId));
        }

        // 2
        public async Task<IEnumerable<TaskDTO>> GetTasksOfUserNameLengthLessFourtyFive(int userId)
        {
            var tasks = await _uow.Tasks.GetAll();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks.Where(t => t.PerformerId == userId && t.Name.Length < 45).ToList());
        }

        // 3
        public async Task<IEnumerable<ShortTaskInfoDTO>> GetTasksOfUserFinishedThisYear(int userId)
        {
            var tasks = await _uow.Tasks.GetAll();

            return tasks.Where(t => t.PerformerId == userId && t.State == TaskState.Finished && t.FinishedAt.Year == 2020)
                        .Select(t => new ShortTaskInfoDTO() { Id = t.Id, Name = t.Name }).ToList();
        }

        public async Task<IEnumerable<TaskDTO>> GetNotFinishedTasksOFUser(int id)
        {
            if (await _uow.Users.GetById(id) == null) throw new NotFoundException(nameof(User), id);

            return _mapper.Map<IEnumerable<TaskDTO>>((await _uow.Tasks.GetAll()).Where(t => t.PerformerId == id && t.State != TaskState.Finished));
        }
    }
}
