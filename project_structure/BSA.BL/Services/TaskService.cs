﻿using AutoMapper;
using BSA.BL.Exceptions;
using BSA.Common.DTO.Task;
using BSA.Repo.Models;
using BSA.Repo.UnitOfWork;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA.BL.Services
{
    public class TaskService
    {
        private readonly UnitOfWork _uow;
        private readonly IMapper _mapper;

        public TaskService(UnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TaskDTO>> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(await _uow.Tasks.GetAll());
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            var task = await _uow.Tasks.GetById(id);
            if (task == null)
                throw new NotFoundException(nameof(TaskEntity), id);

            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<TaskDTO> AddTask(NewTaskDTO task)
        {
            var taskEntity = _mapper.Map<TaskEntity>(task);

            var proj = await _uow.Projects.GetById(task.ProjectId);
            if (proj == null)
                throw new NotFoundException(nameof(Project), task.ProjectId);
            var user = await _uow.Users.GetById(task.PerformerId);
            if (user == null)
                throw new NotFoundException(nameof(User), task.ProjectId);

            await _uow.Tasks.Add(taskEntity);
            await _uow.SaveAsync();

            var newTask = await _uow.Tasks.GetById(taskEntity.Id);
            return _mapper.Map<TaskDTO>(newTask);
        }

        public async Task UpdateTask(NewTaskDTO task, int id)
        {
            var taskCheck = await _uow.Tasks.GetById(id);
            if (taskCheck == null)
                throw new NotFoundException(nameof(TaskEntity), id);
            var proj = await _uow.Projects.GetById(task.ProjectId);
            if (proj == null)
                throw new NotFoundException(nameof(Project), task.ProjectId);
            var user = await _uow.Users.GetById(task.PerformerId);
            if (user == null)
                throw new NotFoundException(nameof(User), task.ProjectId);
            _uow.DetachEntity(taskCheck);

            var taskEntity = _mapper.Map<TaskEntity>(task);
            taskEntity.Id = id;
            taskEntity.CreatedAt = taskCheck.CreatedAt;

            await _uow.Tasks.Update(taskEntity);
            await _uow.SaveAsync();
        }

        public async Task DeleteTask(int id)
        {
            var task = await _uow.Tasks.GetById(id);
            if (task == null)
                throw new NotFoundException(nameof(TaskEntity), id);

            await _uow.Tasks.Remove(id);
            await _uow.SaveAsync();
        }
    }
}
