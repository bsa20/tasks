﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA.BL.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string entityName, int id)
        : base($"Entity {entityName} with id {id} doesn`t exist"){}
    }
}
