﻿using BSA.BL.Services;
using BSA.Common.DTO.Task;
using BSA.Common.DTO.User;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetAll()
        {
            return Ok(await _userService.GetAllUsers());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            return Ok(await _userService.GetUserById(id));
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] NewUserDTO user)
        {
            return Ok(await _userService.AddUser(user));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put([FromBody] NewUserDTO user, int id)
        {
            await _userService.UpdateUser(user, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _userService.DeleteUser(id);
            return StatusCode(204);
        }

        [HttpGet("{userId}/maininfo")]
        public async Task<ActionResult<UserMainInfoDTO>> GetMainInfoAboutUser(int userId)
        {
            return Ok(await _userService.GetMainInformationAboutUser(userId));
        }

        [HttpGet("sorted/tasks/sorted")]
        public async Task<ActionResult<IEnumerable<UserWithTasksDTO>>> GetSortedUsersWithSortedTasks()
        {
            return Ok(await _userService.GetSortedUsersWithSortedTasks());
        }

        [HttpGet("{userId}/tasksInProjects/count")]
        public async Task<ActionResult<IDictionary<int, int>>> GetCountOfTasksOfUserInProject(int userId)
        {
            return Ok(await _userService.GetCountOfTasksOfUserInProject(userId));
        }

        [HttpGet("{userId}/tasks")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasksOfUser(int userId)
        {
            return Ok(await _userService.GetTasksOfUserNameLengthLessFourtyFive(userId));
        }


        [HttpGet("{userId}/thisYear")]
        public async Task<ActionResult<IEnumerable<ShortTaskInfoDTO>>> GetTasksOfUserFinishedThisYear(int userId)
        {
            return Ok(await _userService.GetTasksOfUserFinishedThisYear(userId));
        }

        [HttpGet("{userId}/tasks/NotFinished")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetNotFinishedTasksOFUser(int userId)
        {
            return Ok(await _userService.GetNotFinishedTasksOFUser(userId));
        }
    }
}