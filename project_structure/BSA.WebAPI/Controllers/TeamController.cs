﻿using BSA.BL.Services;
using BSA.Common.DTO.Team;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetAll()
        {
            return Ok(await _teamService.GetAllTeams());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            return Ok(await _teamService.GetTeamById(id));
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Post([FromBody] NewTeamDTO team)
        {
            return Ok(await _teamService.AddTeam(team));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put([FromBody] NewTeamDTO team, int id)
        {
            await _teamService.UpdateTeam(team, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _teamService.DeleteTeam(id);
            return StatusCode(204);
        }

        [HttpGet("users/olderTen")]
        public async Task<ActionResult<IEnumerable<TeamWithUsersDTO>>> GetTeamWithUsersOlderThanTen()
        {
            return Ok(await _teamService.GetTeamsWithUsersOlderThanTenYears());
        }
    }
}