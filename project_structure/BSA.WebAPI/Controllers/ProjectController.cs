﻿using BSA.BL.Services;
using BSA.Common.DTO.Project;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly ProjectService _projService;

        public ProjectController(ProjectService projService)
        {
            _projService = projService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> GetAll()
        {
            return Ok(await _projService.GetAllProjects());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            return Ok(await _projService.GetProjectById(id));
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] NewProjectDTO project)
        {
            return Ok(await _projService.AddProject(project));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put([FromBody] NewProjectDTO project, int id)
        {
            await _projService.UpdateProject(project, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _projService.DeleteProject(id);
            return StatusCode(204);
        }

        [HttpGet("info/main")]
        public async Task<ActionResult<IEnumerable<ProjectMainInfoDTO>>> GetMainInfo()
        {
            return Ok(await _projService.GetMainInfoAboutProjects());
        }
    }
}
