﻿using BSA.BL.Services;
using BSA.Common.DTO.Task;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TaskController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetAll()
        {
            return Ok(await _taskService.GetAllTasks());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            return Ok(await _taskService.GetTaskById(id));
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> Post([FromBody] NewTaskDTO task)
        {
            return Ok(await _taskService.AddTask(task));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put([FromBody] NewTaskDTO task, int id)
        {
            await _taskService.UpdateTask(task, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _taskService.DeleteTask(id);
            return StatusCode(204);
        }
    }
}