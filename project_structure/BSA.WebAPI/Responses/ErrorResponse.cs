﻿using System.Collections.Generic;

namespace BSA.WebAPI.Responses
{
    public class ErrorResponse
    {
        public List<ErrorModel> Errors { get; set; } = new List<ErrorModel>();
    }
}
