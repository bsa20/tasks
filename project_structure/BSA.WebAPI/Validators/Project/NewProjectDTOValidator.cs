﻿using BSA.Common.DTO.Project;
using FluentValidation;
using System;

namespace BSA.WebAPI.Validators.Project
{
    public class NewProjectDTOValidator : AbstractValidator<NewProjectDTO>
    {
        public NewProjectDTOValidator()
        {
            RuleFor(p => p.AuthorId).NotNull().Must(id => id > 0).WithMessage("AuthorId should be more than 0");
            RuleFor(p => p.TeamId).NotNull().Must(id => id > 0).WithMessage("TeamId should be more than 0");
            RuleFor(p => p.Deadline).NotNull().Must(time => time.Ticks > DateTime.Now.Ticks).WithMessage("Deadline should be later than now");
        }
    }
}
