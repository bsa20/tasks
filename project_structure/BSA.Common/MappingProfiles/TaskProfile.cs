﻿using AutoMapper;
using BSA.Common.DTO.Task;
using BSA.Repo.Models;

namespace BSA.Common.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDTO>();
            CreateMap<NewTaskDTO, TaskEntity>();
            CreateMap<TaskDTO, NewTaskDTO>();
        }
    }
}
