﻿using AutoMapper;
using BSA.Common.DTO.Team;
using BSA.Repo.Models;

namespace BSA.Common.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<NewTeamDTO, Team>();
        }
    }
}
