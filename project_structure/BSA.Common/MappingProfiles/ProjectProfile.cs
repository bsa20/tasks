﻿using AutoMapper;
using BSA.Common.DTO.Project;
using BSA.Repo.Models;

namespace BSA.Common.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<NewProjectDTO, Project>();
        }
    }
}
