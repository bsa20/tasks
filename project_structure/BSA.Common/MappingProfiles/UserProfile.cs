﻿using AutoMapper;
using BSA.Common.DTO.User;
using BSA.Repo.Models;

namespace BSA.Common.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<NewUserDTO, User>();
            CreateMap<UserDTO, NewUserDTO>();
        }
    }
}
