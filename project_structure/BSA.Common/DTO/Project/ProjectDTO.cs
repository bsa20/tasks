﻿using BSA.Common.DTO.Task;
using BSA.Common.DTO.Team;
using BSA.Common.DTO.User;
using System;
using System.Collections.Generic;

namespace BSA.Common.DTO.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        public UserDTO Author { get; set; }

        public int TeamId { get; set; }
        public TeamDTO Team { get; set; }

        public IEnumerable<TaskDTO> Tasks { get; set; }
    }
}
