﻿using BSA.Common.DTO.Task;

namespace BSA.Common.DTO.Project
{
    public class ProjectMainInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortetTaskByName { get; set; }
        public int NumberOfUsersInTeam { get; set; }
    }
}
