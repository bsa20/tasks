﻿using BSA.Common.DTO.User;
using System.Collections.Generic;

namespace BSA.Common.DTO.Team
{
    public class TeamWithUsersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDTO> Users { get; set; }
    }
}
