﻿namespace BSA.Common.DTO.Team
{
    public class NewTeamDTO
    {
        public string? Name { get; set; }
    }
}
