﻿using BSA.Common.DTO.Project;
using BSA.Common.DTO.User;
using BSA.Repo.Enums;
using System;

namespace BSA.Common.DTO.Task
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }

        public int ProjectId { get; set; }
        public ProjectDTO Project { get; set; }

        public int PerformerId { get; set; }
        public UserDTO Performer { get; set; }
    }
}
