﻿namespace BSA.Common.DTO.Task
{
    public class ShortTaskInfoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
