﻿using System;

namespace BSA.Common.DTO.Task
{
    public class NewTaskDTO
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime FinishedAt { get; set; }
        public Repo.Enums.TaskState State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
