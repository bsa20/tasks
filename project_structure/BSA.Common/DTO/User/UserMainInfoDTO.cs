﻿using BSA.Common.DTO.Project;
using BSA.Common.DTO.Task;

namespace BSA.Common.DTO.User
{
    public class UserMainInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int NumberOfTasksOfLastProject { get; set; }
        public int NumberOfNotFinishedTasks { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
