﻿using BSA.Common.DTO.Task;
using System.Collections.Generic;

namespace BSA.Common.DTO.User
{
    public class UserWithTasksDTO
    {
        public UserDTO User { get; set; }
        public IEnumerable<TaskDTO> Tasks { get; set; }
    }
}
