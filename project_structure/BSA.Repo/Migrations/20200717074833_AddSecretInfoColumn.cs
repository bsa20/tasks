﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA.Repo.Migrations
{
    public partial class AddSecretInfoColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SecretInfo",
                table: "Projects",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SecretInfo",
                table: "Projects");
        }
    }
}
