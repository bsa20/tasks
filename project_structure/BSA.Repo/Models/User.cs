﻿using Newtonsoft.Json;
using System;

namespace BSA.Repo.Models
{
    public class User
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }

        public int? TeamId { get; set; }
        [JsonIgnore]
        public Team Team { get; set; }
    }
}
