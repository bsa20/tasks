﻿using BSA.Repo.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BSA.Repo.Context
{
    public class BsaDbContext : DbContext
    {
        public BsaDbContext(DbContextOptions<BsaDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();

            modelBuilder.SeedData();
        }

        public DbSet<Project> Projects { get; set;}
        public DbSet<TaskEntity> Tasks { get; set;}
        public DbSet<Team> Teams { get; set;}
        public DbSet<User> Users { get; set;}
    }
}
