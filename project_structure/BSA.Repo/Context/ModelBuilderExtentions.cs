﻿using BSA.Repo.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BSA.Repo.Context
{
    public static class ModelBuilderExtentions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskEntity>()
                .HasOne(t => t.Project)
                .WithMany(p => p.Tasks)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<TaskEntity>()
                .HasOne(t => t.Performer)
                .WithMany()
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany()
                .HasForeignKey(p => p.AuthorId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Project>()
                .HasMany(p => p.Tasks)
                .WithOne(t => t.Project)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany()
                .HasForeignKey(u => u.TeamId)
                .OnDelete(DeleteBehavior.SetNull);
        }


        public static void SeedData(this ModelBuilder modelBuilder)
        {
            var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("..\\BSA.Repo\\data\\projects.json")).ToList();
            var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("..\\BSA.Repo\\data\\users.json")).ToList();
            var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("..\\BSA.Repo\\data\\teams.json")).ToList();
            var tasks = JsonConvert.DeserializeObject<List<Models.TaskEntity>>(File.ReadAllText("..\\BSA.Repo\\data\\tasks.json")).ToList();

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<TaskEntity>().HasData(tasks);
        }
    }
}
