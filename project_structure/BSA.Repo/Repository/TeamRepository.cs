﻿using BSA.Repo.Context;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.Repo.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly BsaDbContext _ctx;

        public TeamRepository(BsaDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task Add(Team entity)
        {
            entity.CreatedAt = DateTime.Now;
            await _ctx.Teams.AddAsync(entity);
        }

        public Task<List<Team>> GetAll()
        {
            return _ctx.Teams.ToListAsync();
        }

        public Task<Team> GetById(int id)
        {
            return _ctx.Teams.FirstOrDefaultAsync(p => p.Id == id);
        }

        public Task Remove(int id)
        {
            return Task.Run(async () => _ctx.Teams.Remove(await GetById(id)));
        }

        public Task Update(Team entity)
        {
            _ctx.Teams.Update(entity);

            return Task.CompletedTask;
        }
    }
}
