﻿using BSA.Repo.Context;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.Repo.Repository
{
    public class TaskRepository : IRepository<TaskEntity>
    {
        private readonly BsaDbContext _ctx;

        public TaskRepository(BsaDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task Add(TaskEntity entity)
        {
            entity.CreatedAt = DateTime.Now;
            await _ctx.Tasks.AddAsync(entity);
        }

        public Task<List<TaskEntity>> GetAll()
        {
            return _ctx.Tasks.Include(t => t.Performer)
                       .ThenInclude(u => u.Team)
                   .Include(t => t.Project)
                       .ThenInclude(p => p.Team)
                   .Include(t => t.Project)
                       .ThenInclude(p => p.Author)
                   .Include(t => t.Project)
                       .ThenInclude(p => p.Tasks)
                           .ThenInclude(t => t.Performer)
                               .ThenInclude(p => p.Team)
                   .ToListAsync();
        }

        public Task<TaskEntity> GetById(int id)
        {
            return _ctx.Tasks.Include(t => t.Performer)
                       .ThenInclude(u => u.Team)
                   .Include(t => t.Project)
                       .ThenInclude(p => p.Team)
                   .Include(t => t.Project)
                       .ThenInclude(p => p.Author)
                   .Include(t => t.Project)
                       .ThenInclude(p => p.Tasks)
                           .ThenInclude(t => t.Performer)
                               .ThenInclude(p => p.Team)
                   .FirstOrDefaultAsync(p => p.Id == id);
        }

        public Task Remove(int id)
        {
            return Task.Run(async () => _ctx.Tasks.Remove(await GetById(id)));
        }

        public Task Update(TaskEntity entity)
        {
            _ctx.Tasks.Update(entity);

            return Task.CompletedTask;
        }
    }
}
