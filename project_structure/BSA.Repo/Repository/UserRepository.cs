﻿using BSA.Repo.Context;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.Repo.Repository
{
    public class UserRepository : IRepository<User>
    {
        private readonly BsaDbContext _ctx;

        public UserRepository(BsaDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task Add(User entity)
        {
            entity.RegisteredAt = DateTime.Now;
            await _ctx.Users.AddAsync(entity);
        }

        public Task<List<User>> GetAll()
        {
            return _ctx.Users.Include(u => u.Team)
                   .ToListAsync();
        }

        public Task<User> GetById(int id)
        {
            return _ctx.Users.Include(u => u.Team)
                   .FirstOrDefaultAsync(p => p.Id == id);
        }

        public Task Remove(int id)
        {
            return Task.Run(async () =>
            {
                _ctx.Tasks.RemoveRange(_ctx.Tasks.Where(t => t.PerformerId == id));
                _ctx.Users.Remove(await GetById(id));
            });
        }

        public Task Update(User entity)
        {
            _ctx.Users.Update(entity);

            return Task.CompletedTask;
        }
    }
}
