﻿using BSA.Repo.Context;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BSA.Repo.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly BsaDbContext _ctx;

        public ProjectRepository(BsaDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task Add(Project entity)
        {
            entity.CreatedAt = DateTime.Now;
            await _ctx.Projects.AddAsync(entity);
        }

        public Task<List<Project>> GetAll()
        {
            return _ctx.Projects.Include(p => p.Tasks)
                       .ThenInclude(t => t.Performer)
                           .ThenInclude(p => p.Team)
                   .Include(p => p.Author)
                       .ThenInclude(a => a.Team)
                   .Include(p => p.Team)
                   .ToListAsync();
        }

        public Task<Project> GetById(int id)
        {
            return _ctx.Projects.Include(p => p.Tasks)
                       .ThenInclude(t => t.Performer)
                           .ThenInclude(p => p.Team)
                   .Include(p => p.Author)
                       .ThenInclude(a => a.Team)
                   .Include(p => p.Team)
                   .FirstOrDefaultAsync(p => p.Id == id);
        }

        public Task Remove(int id)
        {
            return Task.Run(async () => _ctx.Projects.Remove(await GetById(id)));
        }

        public Task Update(Project entity)
        {
           _ctx.Projects.Update(entity);

            return Task.CompletedTask;
        }
    }
}
