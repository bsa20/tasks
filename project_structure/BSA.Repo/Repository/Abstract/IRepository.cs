﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA.Repo.Repository.Abstract
{
    public interface IRepository<TEntity>
    {
        public Task Add(TEntity entity);
        public Task Remove(int id);
        public Task Update(TEntity entity);
        public Task<TEntity> GetById(int id);
        public Task<List<TEntity>> GetAll();
    }
}
