﻿using BSA.Common.DTO.Project;
using BSA.Common.DTO.Task;
using BSA.Common.DTO.Team;
using BSA.Common.DTO.User;
using BSA.Repo.Enums;
using Client.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        private const int DELAY = 3000;

        static void Main(string[] args)
        {
            var projService = new ProjectService();
            var taskService = new TaskService();
            var teamService = new TeamService();
            var userService = new UserService();

            while (true)
            {
                try
                {
                    Task.WaitAll(menu(projService, taskService, teamService, userService));
                    break;
                }
                catch
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error");
                    Console.ResetColor();
                }
            }
        }

        private static async Task menu(ProjectService projeService,
                                 TaskService taskService,
                                 TeamService teamService,
                                 UserService userService)
        {
            while (true)
            {
                Console.WriteLine("\t1  : Create\n" +
                    "\t2  : Read\n" +
                    "\t3  : Update\n" +
                    "\t4  : Delete\n" +
                    "\t5  : Read by id\n" +
                    "\t6  : Get main info abaout all projects (task 7)\n" +
                    "\t7  : Get tasks of user finished in 2020 (task 3)\n" +
                    "\t8  : Get teams with users older than 10 (task 4)\n" +
                    "\t9  : Get main info about user (task 6)\n" +
                    "\t10 : Get users order alphabeticaly with their tasks order by name length (task 5)\n" +
                    "\t11 : Get number of tasks of user in project (task 1)\n" +
                    "\t12 : Get tasks of user with name length <45 (task 2)\n" +
                    " new -> 13 : Mark random task with delay 3s\n" +
                    "\t0  : Exit\n");

                int x = enterNumber();
                if (x == -1) continue;

                switch (x)
                {
                    case 1:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        var entity = enterNewProject();

                                        var result = await projeService.Create(entity);
                                        showProject(result);
                                        break;
                                    }
                                case 2:
                                    {
                                        var entity = enterNewTask();

                                        var result = await taskService.Create(entity);
                                        showTask(result);
                                        break;
                                    }
                                case 3:
                                    {
                                        var entity = enterNewTeam();

                                        var result = await teamService.Create(entity);
                                        showTeam(result);
                                        break;
                                    }
                                case 4:
                                    {
                                        var entity = enterUser();

                                        var result = await userService.Create(entity);
                                        showUser(result);
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 2:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        var result = await projeService.GetAll();
                                        foreach (var item in result)
                                        {
                                            showProject(item);
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        var result = await taskService.GetAll();
                                        foreach (var item in result)
                                        {
                                            showTask(item);
                                        }
                                        break;
                                    }
                                case 3:
                                    {
                                        var result = await teamService.GetAll();
                                        foreach (var item in result)
                                        {
                                            showTeam(item);
                                        }
                                        break;
                                    }
                                case 4:
                                    {
                                        var result = await userService.GetAll();
                                        foreach (var item in result)
                                        {
                                            showUser(item);
                                        }
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 3:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var entity = enterNewProject();

                                        var result = await projeService.Update(entity, id);
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var entity = enterNewTask();

                                        var result = await taskService.Update(entity, id);
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var entity = enterNewTeam();

                                        var result = await teamService.Update(entity, id);
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 4:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var entity = enterUser();

                                        var result = await userService.Update(entity, id);
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 4:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var result = await projeService.Delete(id);
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var result = await taskService.Delete(id);
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var result = await teamService.Delete(id);
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                case 4:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;

                                        var result = await userService.Delete(id);
                                        if (result == "") Console.WriteLine("Seccess");
                                        else Console.WriteLine(result);
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 5:
                        {
                            x = selectEntity();
                            if (x == -1) continue;

                            switch (x)
                            {
                                case 1:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;
                                        ProjectDTO result = null;
                                        try
                                        {
                                            result = await projeService.GetById(id);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }
                                        if (result != null) showProject(result);
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;
                                        TaskDTO result = null;
                                        try
                                        {
                                            result = await taskService.GetById(id);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }
                                        if (result != null) showTask(result);
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;
                                        TeamDTO result = null;
                                        try
                                        {
                                            result = await teamService.GetById(id);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }
                                        if (result != null) showTeam(result);
                                        break;
                                    }
                                case 4:
                                    {
                                        Console.Write("id: ");
                                        var id = enterNumber();
                                        if (id == -1) continue;
                                        UserDTO result = null;
                                        try
                                        {
                                            result = await userService.GetById(id);
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }
                                        if (result != null) showUser(result);
                                        break;
                                    }
                                default:
                                    break;
                            }

                            break;
                        }
                    case 6:
                        {
                            var result = await projeService.GetProjectsMainInfo();
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Project id: {item.Project.Id}, Name: {item.Project.Name}");
                                if (item.LongestTaskByDescription != null)
                                {
                                    Console.WriteLine($"Longest task of project by description: id({item.LongestTaskByDescription.Id})\n" +
                                                        $"Shortest task of project by name: id({item.ShortetTaskByName.Id})");
                                }
                                Console.WriteLine($"Number of users in team of project(description > 20 and count of tasks < 3): {item.NumberOfUsersInTeam}\n");
                            }
                            break;
                        }
                    case 7:
                        {
                            Console.WriteLine("Enter id of user");
                            var id = enterNumber();
                            if (id == -1) continue;

                            var result = await userService.GetTasksOfUserFinishedThisYear(id);
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Task id: {item.Id}, Name: {item.Name}");
                            }
                            if (result.Count() == 0) Console.WriteLine("None");

                            break;
                        }
                    case 8:
                        {
                            var result = await teamService.GetTeamWithUsersOlderThanTen();
                            foreach (var team in result)
                            {
                                Console.WriteLine($"Team id: {team.Id}, Team: {team.Name}");
                                foreach (var user in team.Users)
                                {
                                    Console.WriteLine($"\t{user.FirstName} {user.LastName}, registered at: {user.RegisteredAt}");
                                }
                            }
                            break;
                        }
                    case 9:
                        {
                            Console.WriteLine("Enter id of user");
                            var id = enterNumber();
                            if (id == -1) continue;
                            var result = await userService.GetMainInfoAboutUser(id);

                            Console.WriteLine($"{result.User.FirstName} {result.User.LastName}");
                            if (result.LastProject != null)
                            {
                                Console.WriteLine($"Last project: id ({result.LastProject.Id}) name ({result.LastProject.Name})\n" +
                                                  $"Number of taks on last project: {result.NumberOfTasksOfLastProject}");
                            }
                            Console.WriteLine($"Number of not finished tasks: {result.NumberOfNotFinishedTasks}\n" +
                                              $"Longest task: id ({result.LongestTask.Id}) name ({result.LongestTask.Name})\n");
                            break;
                        }
                    case 10:
                        {
                            var result = await userService.GetSortedUsersWithSortedTasks();
                            foreach (var item in result)
                            {
                                Console.WriteLine($"{item.User.FirstName} {item.User.LastName}");
                                foreach (var task in item.Tasks)
                                {
                                    Console.WriteLine($"\tTask: id({task.Id}) {task.Name}");
                                }
                            }
                            break;
                        }
                    case 11:
                        {
                            Console.WriteLine("Enter id of user");
                            var id = enterNumber();
                            if (id == -1) continue;

                            var result = await userService.GetCountOfTasksOfUserInProject(id);
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Project id: {item.Key}, count of tasks: {item.Value}");
                            }
                            break;
                        }
                    case 12:
                        {
                            Console.WriteLine("Enter id of user");
                            var id = enterNumber();
                            if (id == -1) continue;

                            var result = await userService.GetTasksOfUser(id);
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Task id: {item.Id}, Project id: {item.ProjectId}, Name: {item.Name}");
                            }
                            if (result.Count() == 0) Console.WriteLine("None");
                            break;
                        }
                    case 13:
                        {
                            markRandomTaskAsFinished(taskService);
                            break;
                        }
                    case 0:
                        {
                            return;
                        }
                }
                Console.WriteLine("\nPress any key to return in menu");
                Console.ReadKey();
                Console.Clear();
            }
        }

        private static async void markRandomTaskAsFinished(TaskService taskService)
        {
            try
            {
                var taskId = await taskService.MarkRandomTaskWithDelay(DELAY);

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Task with id: {taskId} was finished");
                Console.ResetColor();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
                Console.ResetColor();
            }
        }

        private static int enterNumber()
        {
            int x;
            try { x = int.Parse(Console.ReadLine().Trim()); }
            catch
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Enter number");
                Console.ResetColor();
                return -1;
            }
            return x;
        }

        private static int selectEntity()
        {
            Console.WriteLine("\t1 : Project\n" +
                                "\t2 : Task\n" +
                                "\t3 : Team\n" +
                                "\t4 : User\n" +
                                "\t0 : Exit");
            int x;
            try { x = int.Parse(Console.ReadLine().Trim()); }
            catch
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Enter number");
                Console.ResetColor();
                return -1;
            }
            return x;
        }

        private static void showTeam(TeamDTO team)
        {
            Console.WriteLine();
            Console.WriteLine($"id: {team.Id}\n" +
                $"name: {team.Name}\n");
        }

        private static void showTask(TaskDTO task)
        {
            Console.WriteLine();
            Console.WriteLine($"id: {task.Id}\n" +
                $"name: {task.Name}\n" +
                $"performer id: {task.PerformerId}\n" +
                $"finished at: {task.FinishedAt}\n");
        }

        private static void showProject(ProjectDTO proj)
        {
            Console.WriteLine();
            Console.WriteLine($"id: {proj.Id}\n" +
                $"name: {proj.Name}\n" +
                $"authorId: {proj.AuthorId}\n" +
                $"teamId: {proj.TeamId}\n" +
                $"deadline: {proj.Deadline}\n");
        }

        private static void showUser(UserDTO user)
        {
            Console.WriteLine();
            Console.WriteLine($"id: {user.Id}\n" +
                $"first name: {user.FirstName}\n" +
                $"last name: {user.LastName}\n" +
                $"email: {user.Email}" +
                $"birthday: {user.Birthday}\n");
        }

        private static NewUserDTO enterUser()
        {
            Console.Write("First Name: ");
            var fName = Console.ReadLine();
            Console.Write("LastName Name: ");
            var lName = Console.ReadLine();
            Console.Write("Email Name: ");
            var email = Console.ReadLine();
            Console.WriteLine("Birthday: ");
            var birth = enterDate();
            Console.Write("TeamId: ");
            var teamId = int.Parse(Console.ReadLine().Trim());

            var entity = new NewUserDTO
            {
                FirstName = fName,
                LastName = lName,
                Email = email,
                Birthday = birth,
                TeamId = teamId
            };

            return entity;
        }

        private static NewTeamDTO enterNewTeam()
        {
            Console.Write("Name: ");
            var name = Console.ReadLine();

            var entity = new NewTeamDTO
            {
                Name = name
            };
            return entity;
        }

        private static NewTaskDTO enterNewTask()
        {
            Console.Write("Name: ");
            var name = Console.ReadLine();
            Console.Write("Description: ");
            var desc = Console.ReadLine();
            Console.WriteLine("FinishedAt: ");
            var deadline = enterDate();
            Console.Write("PerforemrId: ");
            var perforemerId = int.Parse(Console.ReadLine().Trim());
            Console.Write("ProjectId: ");
            var projId = int.Parse(Console.ReadLine().Trim());
            Console.Write("State(0 - 3)");
            var state = int.Parse(Console.ReadLine().Trim());

            var entity = new NewTaskDTO()
            {
                Name = name,
                Description = desc,
                FinishedAt = deadline,
                PerformerId = perforemerId,
                ProjectId = projId,
                State = (TaskState)state
            };
            return entity;
        }

        private static NewProjectDTO enterNewProject()
        {
            Console.Write("Name: ");
            var name = Console.ReadLine();
            Console.Write("Description: ");
            var desc = Console.ReadLine();
            Console.WriteLine("Deadlene: ");
            var deadline = enterDate();
            Console.Write("AuthorId: ");
            var authorId = int.Parse(Console.ReadLine().Trim());
            Console.Write("TeamId: ");
            var teamId = int.Parse(Console.ReadLine().Trim());

            var entity = new NewProjectDTO()
            {
                Name = name,
                Description = desc,
                AuthorId = authorId,
                TeamId = teamId,
                Deadline = deadline
            };
            return entity;
        }

        private static DateTime enterDate()
        {
            Console.Write("Enter a day: ");
            int day = int.Parse(Console.ReadLine().Trim());
            Console.Write("Enter a month: ");
            int month = int.Parse(Console.ReadLine().Trim());
            Console.Write("Enter a year: ");
            int year = int.Parse(Console.ReadLine().Trim());

            return new DateTime(year, month, day);
        }
    }
}
