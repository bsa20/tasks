﻿namespace Client.API
{
    public class ApiOptions
    {
        public string GetProjectSufix { get; } = "api/project";
        public string PostProjectSufix { get; } = "api/project";
        public string PutProjectSufix { get; } = "api/project";
        public string DeleteProjectSufix { get; } = "api/project";
        public string GetProjectsMainInfo { get; } = "api/project/info/main";

        public string GetTaskSufix { get; } = "api/task";
        public string PostTaskSufix { get; } = "api/task";
        public string PutTaskSufix { get; } = "api/task";
        public string DeleteTaskSufix { get; } = "api/task";

        public string GetTeamSufix { get; } = "api/team";
        public string PostTeamSufix { get; } = "api/team";
        public string PutTeamSufix { get; } = "api/team";
        public string DeleteTeamSufix { get; } = "api/team";
        public string GetTeamWithUsersOlderThanTen { get; } = "api/team/users/olderTen";

        public string GetUserSufix { get; } = "api/user";
        public string PostUserSufix { get; } = "api/user";
        public string PutUserSufix { get; } = "api/user";
        public string DeleteUserSufix { get; } = "api/user";
        public string GetTasksOfUserFinishedThisYear { get { return "api/user/" + UserId + "/thisYear"; } }
        public string GetMainInfoAboutUser { get { return "api/user/" + UserId + "/maininfo"; } }
        public string GetSortedUsersWithSortedTasks { get; } = "api/user/sorted/tasks/sorted";
        public string GetCountOfTasksOfUserInProject { get { return "api/user/" + UserId + "/tasksInProjects/count"; } }
        public string GetTasksOfUser { get { return "api/user/" + UserId + "/tasks"; } }

        public int UserId { get; set; } = -1;
    }
}
