﻿using AutoMapper;
using BSA.Common.MappingProfiles;
using BSA.Common.DTO.Task;
using BSA.Repo.Enums;
using Client.API;
using Client.Services.HttpServices.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Client.Services
{
    public class TaskService : HttpService<TaskDTO, NewTaskDTO>
    {
        private readonly ApiOptions _apiOptions;

        public TaskService()
        {
            _apiOptions = new ApiOptions();
        }

        public async Task<IEnumerable<TaskDTO>> GetAll()
        {
            return await this.GetAllEntities(_apiOptions.GetTaskSufix);
        }

        public async Task<TaskDTO> GetById(int id)
        {
            return await this.GetEntityById(_apiOptions.GetTaskSufix, id);
        }

        public async Task<TaskDTO> Create(NewTaskDTO task)
        {
            return await this.PostRequest(_apiOptions.PostTaskSufix, task);
        }

        public async Task<string> Update(NewTaskDTO task, int id)
        {
            return await this.PutRequest(_apiOptions.PutTaskSufix, id, task);
        }

        public async Task<string> Delete(int id)
        {
            return await this.DeleteRequest(_apiOptions.DeleteTaskSufix, id);
        }

        public Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var tsc = new TaskCompletionSource<int>();
            var timer = new System.Timers.Timer(delay);

            timer.Elapsed += async (sender, e) =>
            {
                try
                {
                    timer.Stop();
                    timer.Dispose();

                    var taskId = await markRandomTaskAsFinished();

                    tsc.SetResult(taskId);
                }
                catch (Exception exeption)
                {
                    tsc.SetException(exeption);
                }
            };
            timer.Start();

            return tsc.Task;
        }

        private async Task<int> markRandomTaskAsFinished()
        {
            var rand = new Random();
            var mpCfg = new MapperConfiguration(cfg => cfg.AddProfile<TaskProfile>());
            var mapper = new Mapper(mpCfg);

            var allTasks = (await this.GetAll()).Where(t => t.State != TaskState.Finished).ToArray();
            var randomTask = allTasks[rand.Next(0, allTasks.Length)];
            var updatedTask = mapper.Map<NewTaskDTO>(randomTask);
            updatedTask.State = TaskState.Finished;

            var response = await this.Update(updatedTask, randomTask.Id);

            return randomTask.Id;
        }
    }
}
