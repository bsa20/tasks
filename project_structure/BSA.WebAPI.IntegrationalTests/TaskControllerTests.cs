﻿using BSA.Common.DTO.Task;
using BSA.Repo.Enums;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BSA.WebAPI.IntegrationalTests
{
    public class TaskControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private HttpClient _client;
        private const string URL = "https://localhost:44337";

        public TaskControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task DeleteTask_WhenTaskExist_ThenResponseStatusCodeNoContent()
        {
            var newTask = new NewTaskDTO
            {
                Name = "New task",
                Description = "New Description",
                FinishedAt = new DateTime(2021, 3, 15),
                State = TaskState.Created,
                PerformerId = 49,
                ProjectId = 80
            };

            var jsonString = JsonConvert.SerializeObject(newTask);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var addResponse = await _client.PostAsync(URL + "/api/task", content);
            var addResultJson = await addResponse.Content.ReadAsStringAsync();
            var addedObj = JsonConvert.DeserializeObject<TaskDTO>(addResultJson);

            var delResponse = await _client.DeleteAsync(URL + "/api/task/" + addedObj.Id);

            Assert.Equal(HttpStatusCode.NoContent, delResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenTaskDoesNotExist_ThenResponseStatusCodeNotFound()
        {
            var delResponse = await _client.DeleteAsync(URL + "/api/task/" + int.MaxValue);

            Assert.Equal(HttpStatusCode.NotFound, delResponse.StatusCode);
        }
    }
}
