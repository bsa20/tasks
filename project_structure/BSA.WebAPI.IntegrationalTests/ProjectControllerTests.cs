﻿using BSA.Common.DTO.Project;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BSA.WebAPI.IntegrationalTests
{
    public class ProjectControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private HttpClient _client;
        private const string URL = "https://localhost:44337";

        public ProjectControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Theory]
        [InlineData("{\"Name\":\"New Project\",\"Description\":\"New Description\",\"AuthorId\":34,\"TeamId\":6,\"Deadline\":\"2021-09-05T12:42:26.2763701+00:00\"}")]
        [InlineData("{\"Name\":\"New Project 2\",\"AuthorId\":48,\"TeamId\":5,\"Deadline\":\"2022-10-09T12:42:26.2763701+00:00\"}")]
        public async Task AddProject_WhenValidData_ThenProjectDTOAndStatusCodeOk(string jsonString)
        {
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(URL + "/api/project", content);
            string result = await response.Content.ReadAsStringAsync();
            var oldObj = JsonConvert.DeserializeObject<NewProjectDTO>(jsonString);
            var obj = JsonConvert.DeserializeObject<ProjectDTO>(result);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.NotNull(obj);
            Assert.Equal(oldObj.Name, obj.Name);
        }

        [Theory]
        [InlineData("{\"Name\":\"New Project\",\"Description\":\"New Description\",\"AuthorId\":34,\"TeamId\":1568,\"Deadline\":\"2021-09-05T12:42:26.2763701+00:00\"}")]
        [InlineData("{\"Name\":\"New Project 2\",\"AuthorId\":158,\"TeamId\":5,\"Deadline\":\"2022-10-09T12:42:26.2763701+00:00\"}")]
        public async Task AddProject_WhenAuthorOrTeamDoesNotExist_ThenStatusCodeNotFound(string jsonString)
        {
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(URL + "/api/project", content);
            string result = await response.Content.ReadAsStringAsync();

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [InlineData("{\"Name\":\"New Project 2\",\"Description\":\"New Description\",\"AuthorId\":10,\"TeamId\":5}")]
        [InlineData("{\"Name\":\"New Project 2\",\"TeamId\":5,\"Deadline\":\"2022-10-09T12:42:26.2763701+00:00\"}")]
        public async Task AddProject_WhenDataNotValid_ThenStatusCodeBadRequest(string jsonString)
        {
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(URL + "/api/project", content);
            string result = await response.Content.ReadAsStringAsync();

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
