﻿using BSA.Common.DTO.Team;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BSA.WebAPI.IntegrationalTests
{
    public class TeamControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private HttpClient _client;
        private const string URL = "https://localhost:44337";

        public TeamControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Theory]
        [InlineData("{\"name\":\"Team 1\"}")]
        [InlineData("{\"name\":\"Team 2\"}")]
        [InlineData("{\"name\":\"Team 3\"}")]
        public async Task AddTeam_WhenDataValid_ThenStatusCodeOkAndTeamDTO(string jsonString)
        {
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(URL + "/api/team", content);
            string result = await response.Content.ReadAsStringAsync();
            var oldObj = JsonConvert.DeserializeObject<NewTeamDTO>(jsonString);
            var obj = JsonConvert.DeserializeObject<TeamDTO>(result);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.NotNull(obj);
            Assert.Equal(oldObj.Name, obj.Name);
        }
    }
}
